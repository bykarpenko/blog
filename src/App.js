import { useState } from 'react';
import './App.scss';
import Auth from './views/auth';
import Notification from './component/Notification';
import Landing from './views/landing';

import useAuth from './hooks/useAuth';

const App = () => {
  const { isAuth } = useAuth();
  const [notify, setNotify] = useState([]);

  const createNotify = data => {
    setNotify(prev => [...prev, data]);
    setTimeout(() => {
      setNotify(prev => prev.splice(1));
    }, 3000);
  };

  return (
    <div className="bgApp">
      {!isAuth && <Auth createNotify={createNotify} />}
      {isAuth && <Landing />}
      <div className="wrapperNotification">
        {notify.map((iteam, i) => (
          <Notification
            title={iteam.title}
            message={iteam.message}
            color={iteam.color}
            key={i}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
