import { useState } from 'react';
import useAuth from '../../../hooks/useAuth';
import './style.scss';
import { ReactComponent as LogoImg } from '../../../assets/photos.svg';

const SignIn = ({ createNotify, setState }) => {
  
  const { login } = useAuth();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // const submit = e => {
  //   e.preventDefault();
  //   console.log(email, password);

  //   axios
  //     .post('https://22k.space/api/auth/login', { email, password })
  //     .then(res => {
  //       setIsAuthenticated(true);
  //       localStorage.setItem('isAuthenticated', res.data.accessToken);
  //       setUser(res.data.user);
  //       setArticle(res.data.article);
  //       setUsers(res.data.users);

  //     })
  //     .catch(err => {
  //       createNotify({
  //         title: 'Login failed',
  //         message: 'Incorrect login or password',
  //         color: '#f44336',
  //       });
  //     });
  // };

  const submit = async e => {
    e.preventDefault();
    try {
      await login(email, password);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="containerForm">
      <form className="iteamForm" onSubmit={submit}>
        <div className="iteamLogoImg">
          <LogoImg />
        </div>

        <h3>Login to your Account</h3>

        <div className="inputWrapper">
          <label>Email:</label>
          <input
            value={email}
            onChange={e => setEmail(e.target.value)}
            type="email"
            name="name"
            required
          />
        </div>

        <div className="inputWrapper">
          <label>Password:</label>
          <input
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
            name="name"
            required
          />
        </div>

        <button className="iteamAddSingIn">Sign In</button>

        <div className="iteamTab">
          {' '}
          <div
            onClick={() => {
              setState('SignUp');
            }}
          >
            Don't have an account? Sign Up
          </div>
        </div>

        <div className="coppyright">22karpenko © Your Website 2021.</div>
      </form>
    </div>
  );
};

export default SignIn;
