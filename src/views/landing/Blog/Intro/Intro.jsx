import './style.scss';
import { ReactComponent as LogoImg } from '../../../../assets/photos.svg';

const Intro = () => {
  return (
    <div className="introBlog">
      <div className="intro">
        <h1>Welcome to</h1>
        <div className="introImgTitle">
          <div className="introLogoImg">
            <LogoImg />
          </div>
          <div className="introTitle">My Blog</div>
        </div>
      </div>

      <div className="scroll-down">
        <span></span>
        <span></span>
        <span></span>
      </div>

      {/* <section id="section10" className="demo">
            <a href="#posts"><span></span>Scroll</a>
            </section> */}
    </div>
  );
};

export default Intro;
