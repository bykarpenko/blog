import Intro from './Intro';
import Post from './Post';
import useAuth from '../../../hooks/useAuth';
import './style.scss';

const Blog = () => {
  const { article } = useAuth();

  return (
    <div>
      <Intro />
      <div id="posts" className="wrapperBlog">
        {[
          {
            cover:
              'https://st2.depositphotos.com/1030956/7518/v/600/depositphotos_75185843-stock-illustration-blue-plus-sign.jpg',
            title: '+Add New Post',
            description: 'Short description of new article',
          },
          ...article,
        ].map((iteam, i) => (
          <Post iteam={iteam} key={iteam._id || 'newpost'} />
        ))}
      </div>
    </div>
  );
};

export default Blog;
