import Avatar from '../../../../component/avatar';

import './style.scss';

const ItemComment = ({ item }) => {
  return (
    <div className="wrapperComment">
      <div className="containerAuthorComment">
        <div className="authorPostPhoto">
          <Avatar user={item?.author} />
        </div>

        <div>
          <div className="authorCommentName">
            {item?.author.name + ' ' + item?.author.surName}
          </div>
          <div className="authorCommentDate">
            {' '}
            {new Date(Number(item?.createdAt)).toLocaleString()}
          </div>
        </div>
      </div>
      <div className="commentItem">{item.body}</div>
    </div>
  );
};
export default ItemComment;
