import { useHistory } from 'react-router-dom';
import { useState } from 'react';
import Post from '../Blog/Post';
import './style.scss';
import useAuth from '../../../hooks/useAuth';

const PageCreatePost = () => {
  const { post } = useAuth();

  const [file, setFile] = useState(null);
  const [titlePost, setTitlePost] = useState(null);
  const [titleBody, setBodyPost] = useState(null);

  const history = useHistory();
  const handleHistory = () => {
    history.push('/');
  };

  const submitPost = () => {
    post.add(file, titlePost, titleBody);
    handleHistory();
  };

  return (
    <div className="wrapperPageCreatePost">
      <div className="infoPageCreatePost">
        <div className="infoContainer">
          <h3>Create New Post </h3>

          <div className="fileCreateModal">
            <input
              type="file"
              id="file"
              onChange={e => setFile(e.target.files[0])}
              accept=".jpg, .png, .jpeg"
              required
            />
          </div>
          <div className="titleCreateModal">
            <label>Title New Post:</label>
            <input
              type="text"
              name="name"
              onChange={e => setTitlePost(e.target.value)}
              required
            />
          </div>
          <div className="bodyCreateModal">
            <label>Body New Post:</label>
            <textarea
              onChange={e => {
                setBodyPost(e.target.value);
              }}
              required
            />
          </div>
          <div className="toPublishButton" onClick={submitPost}>
            to publish
          </div>
        </div>
      </div>

      <Post
        iteam={{
          cover: !file
            ? 'https://st2.depositphotos.com/1030956/7518/v/600/depositphotos_75185843-stock-illustration-blue-plus-sign.jpg'
            : window.URL.createObjectURL(file),
          title: !titlePost ? ' Title New Post' : titlePost,
          description: !titleBody
            ? 'Short description of new article'
            : titleBody,
        }}
      />
    </div>
  );
};

export default PageCreatePost;
