import Blog from './Blog';
import Header from './Header';
import Profile from './Profile';
import './style.scss';
import { Route } from 'react-router-dom';
import PagePost from './PagePost';
import PageCreatePost from './PageAddPost';
import PageAdmin from './PageAdmin';

const Landing = () => {
  return (
    <div className="wrapperLanding">
      <Header />
      <Route exact path="/" render={() => <Blog />} />
      <Route path="/profile" render={() => <Profile />} />
      <Route path="/post/:id" render={() => <PagePost />} />
      <Route path="/create_post" render={() => <PageCreatePost />} />
      <Route path="/admin" render={() => <PageAdmin />} />
    </div>
  );
};

export default Landing;
