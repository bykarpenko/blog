import { createContext, useEffect, useReducer } from 'react';
import jwtDecode from 'jwt-decode';
import { url } from '../config';
import axios from '../utils/axios';

const initialState = {
  isAuth: false,
  isInitial: false,
  user: null,
  users: [],
  article: [],
};

const isValidToken = accessToken => {
  if (!accessToken) {
    return false;
  }
  const decoded = jwtDecode(accessToken);
  const currentTime = Date.now() / 1000;

  return decoded.exp > currentTime;
};

const setSession = accessToken => {
  if (accessToken) {
    localStorage.setItem('tokenToken', accessToken);
    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
  } else {
    localStorage.removeItem('tokenToken');
    delete axios.defaults.headers.common.Authorization;
  }
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return {
        ...state,
        isAuth: true,
        isInitial: true,
        user: action.payload.user,
        users: action.payload.users,
        article: action.payload.article,
      };
    }

    case 'LOGOUT': {
      return {
        ...state,
        isAuth: false,
        user: null,
        users: [],
        article: [],
      };
    }

    case 'INIT': {
      return {
        ...state,
        isInitial: true,
      };
    }

    case 'CHANGEPROFILEAVATAR': {
      return {
        ...state,
        user: action.payload,
      };
    }

    case 'CHANGEPROFILEUSERDATA': {
      return {
        ...state,
        user: action.payload,
      };
    }

    case 'POSTADD': {
      return {
        ...state,
        article: [action.payload, ...state.article],
      };
    }

    case 'POSTRM': {
      return {
        ...state,
        article: state.article.filter(iteam => {
          return iteam._id !== action.payload._id;
        }),
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
};

const AuthContext = createContext({
  ...initialState,
  login: () => {},
  logout: () => {},
  post: () => {},
  avatarChange: () => {},
  userDataChange: () => {},
});

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const initial = async () => {
      try {
        const accessToken = window.localStorage.getItem('tokenToken');

        if (accessToken && isValidToken(accessToken)) {
          setSession(accessToken);

          const res = await axios.get(`${url}/api/auth/initialization`);

          const { user, users, article } = res.data;

          dispatch({
            type: 'LOGIN',
            payload: {
              user,
              users,
              article,
            },
          });
        } else {
          dispatch({
            type: 'INIT',
          });
        }
      } catch (err) {
        console.log(err);
        setSession(null);
        dispatch({ type: 'LOGOUT' });
      }
    };
    initial();
  }, []);

  if (!state.isInitial) {
    return (
      <div className="loader">
        <img
          src="https://i.pinimg.com/originals/d2/20/b2/d220b28ec97e9c4d0ea33e3dd45b1d70.gif"
          alt="loader"
        />
      </div>
    );
  }

  const login = async (email, password) => {
    const res = await axios.post(`${url}/api/auth/login`, { email, password });

    const { accessToken, user, users, article } = res.data;

    setSession(accessToken);

    dispatch({
      type: 'LOGIN',
      payload: {
        user,
        users,
        article,
      },
    });
  };

  const logout = () => {
    setSession(null);
    dispatch({ type: 'LOGOUT' });
  };

  const avatarChange = async fileAvatar => {
    const formData = new FormData();
    formData.append('avatar', fileAvatar);
    const res = await axios.put(`${url}/api/user`, formData);

    dispatch({
      type: 'CHANGEPROFILEAVATAR',
      payload: res.data,
    });
  };

  const userDataChange = async (name, surName, phone) => {
    const res = await axios.put(`${url}/api/user`, { name, surName, phone });

    dispatch({
      type: 'CHANGEPROFILEUSERDATA',
      payload: res.data,
    });
  };

  const post = {
    add: async (file, titlePost, titleBody) => {
      const formData = new FormData();
      formData.append('cover', file);
      formData.append('title', titlePost);
      formData.append('description', titleBody);
      const res = await axios.post(`${url}/api/article/`, formData);

      dispatch({
        type: 'POSTADD',
        payload: res.data,
      });
    },

    rm: async id => {
      const res = await axios.delete(`${url}/api/article/` + id);

      const { _id } = res.data;

      dispatch({
        type: 'POSTRM',
        payload: {
          _id,
        },
      });
    },
  };

  return (
    <AuthContext.Provider
      value={{ ...state, login, logout, post, avatarChange, userDataChange }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
